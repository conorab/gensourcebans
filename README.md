# genSourceBans

## Description

This will generate an SRCDS source bans config based on the CSV file specified in the the PHP file. The purpose of this is to allow SRCDS servers running in Docker to fetch their bans from a server with persistent storage while also making it easy to manage the list of bans using a spreadsheet. While this script was originally designed to run on a web server, it is now done by running the PHP program with php-cli and sending the output to the container using Docker Exec. As a result, the authentication for running this PHP program has been removed.

## Additional Details

Unfortunately, SRCDS (at least the version used for Left 4 Dead 2) does not store non-permanent bans across restarts, so bans lasting a week or so will disappear. Additionally, if you are running a server in a Docker container, the bans may disappear next time the container is re-built. By having the server periodically fetch the bans from a PHP page, you can have the bans stored outside the container (also meaning multiple servers can use the same ban list) as well as store non-permanent bans.

By tracking the start date of a ban and it's duration, 'genSourceBans' is able to generate a ban config which adjusts the remaining length of the ban on each run, while also providing lists of permanent bans. Bans which are past expiry can be kept in the CSV and 'genSourceBans' will just ignore these when generating the ban config, allowing you to keep historical bans on record for the future. The description of each ban is also added to the configuration as a comment in case it needs to be reviewed on the live server.

## Bans Format

The following is a sample of Bans.csv:

	Steam ID,Manually Unbanned,Length,Reason,Ban Date	
	STEAM_1:1:00000000,0,10080,Team sabotage,1589452800
	STEAM_1:0:00000000,1,0,Hacking,1589452800

Each row contains (in order), the Steam ID is the player being banned, whether they have been manually unbanned ('0' for no, '1' for yes), the length of the ban in minutes, the reason (a description) and the ban date expressed as seconds since the UNIX epoch. In the example, the first ban has not been manually un-done (second column is '0'), the ban duration is 1 week ('10080' minutes), the ban reason is 'Team sabotage' and the ban date is 'Thu May 14 20:40:00 AEST 2020' ('1589452800' seconds since the UNIX Epoch). The second ban has been manually un-done (second column is '1') but would have otherwise been permanent (third column is '0'), the ban reason is 'Hacking' and the ban date is 'Thu May 14 20:40:00 AEST 2020' ('1589452800' seconds since UNIX Epoch).

## Supported Services

In addition, the two following files are provided as part of 'genSourceBans':
* systemd-system/updateBannedUsers_containerNameHere.service
* systemd-system/updateBannedUsers_containerNameHere.timer

The former is a SystemD service which will execute genSourceBans with php-cli and output the result to the banned user config in the 'containerNameHere' container. Your SRCDS server should be configured to reload this configuration on every map change so the bans are updated frequently. The latter is a SystemD timer which will trigger the SystemD service every 10 minutes.

