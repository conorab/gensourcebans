<?php
	# Exit Codes

	# 1 - Removed in V2 - Failed to load globalVariables.php.
	# 2 - One or more required variables not set.
	# 3 - One or more string-only variables are not strings.
	# 4 - Removed in V2 - Failed to authenticate; null secret.
	# 5 - Removed in V2 - Failed to authenticate; incorrect secret.
	# 6 - Removed in V2 - authSecret not set.
	# 7 - Removed in V2 - authSecret is not a string.
	# 8 - Removed in V2 - Failed to authenticate; given secret not a string.
	# 9 - CSV header cell values do not match expected values.
	# 10 - Row must contain exactly 5 cells.
	# 11 - Delimiter variables contain more than 1 character.
	# 12 - 'banSourcePath refers to a file that does not exist.

	$banSourcePath='Bans.csv';

	$errorForBanConfig='# Error encountered while generating ban config.';

	header('Content-Type:text/plain');

	$banDelimiterChar=',';
	$banQuoteChar='"';

	$variablesToCheck=array('banSourcePath','banDelimiterChar','banQuoteChar');
	foreach ($variablesToCheck as &$i) {

		if (! isset(${$i})) {
			error_log("$i variable not set.");
			echo $errorForBanConfig;
			exit(2);
		}
	}
	unset($variablesToCheck);

	$variablesToCheck=array('banSourcePath','banDelimiterChar','banQuoteChar');
	foreach ($variablesToCheck as &$i) {

		if (! gettype(${$i}) == 'string') {
			error_log("$i is not a string");
			echo $errorForBanConfig;
			exit(3);
		}
	}
	unset($variablesToCheck);

	$variablesToCheck=array('banDelimiterChar','banQuoteChar');
	foreach ($variablesToCheck as &$i) {

		if (! (strlen(${$i}) === 1)) {
			error_log("$i contains more than 1 character.");
			echo $errorForBanConfig;
			exit(11);
		}
	}
	unset($variablesToCheck);

	if (! file_exists($banSourcePath)) {
		error_log('banSourcePath refers to a file which does not exist.');
		echo $errorForBanConfig;
		exit(12);
	}

	$timeSinceEpoch=time();

	$banSourceContents=fopen($banSourcePath,'r',);

	$banOutput='';
	$i_procCount=0;
	while ( ($i = fgetcsv($banSourceContents,0,$banDelimiterChar,$banQuoteChar)) !== FALSE) {

		# Cells
		# 0 - Steam ID
		# 1 - Manually unbanned? (0 = no, 1 = yes)
		# 2 - Length of the ban in minutes.
		# 3 - Ban reason
		# 4 - Date of ban (seconds since epoch)

		if (! (count($i) === 5)) {
			error_log('Row must contain exactly 5 cells.');
			echo $errorForBanConfig;
			exit(10);
		}

		# We check if $i_procCount is 0 to not evaluate bans for the header row and do some extra error checking.

		if ($i_procCount == 0) {
			if (! (($i[0] === 'Steam ID') and ($i[1] === 'Manually Unbanned') and ($i[2] === 'Length') and ($i[3] === 'Reason') and ($i[4] === 'Ban Date'))) {
				error_log('CSV header cell contents do not match expect values.');
				echo $errorForBanConfig;
				exit(9);
			}

		} else {
			
			# If cell 1 is null, skip this row since the user was manually unbanned.

			if ($i[1] == 0) {
				if ($i[2] == 0) {
					$banOutput=$banOutput.'// '.$i[3]."\nbanid 0 ".$i[0]."\n";
				} else {
					$remainingTime=((($i[2]*60)+$i[4])-$timeSinceEpoch);

					if ($remainingTime > 0) {
						$banOutput=$banOutput.'// '.$i[3]."\nbanid ".round($remainingTime/60).' '.$i[0]."\n";
					}
				}
			}
		}

		$i_procCount=$i_procCount+1;
	}	
	fclose($banSourceContents);

	echo $banOutput;

?>
